#ifndef _UTIL_H
#define	_UTIL_H

#define INC_WRAP(a,l)		\
	do {					\
		++(a);				\
		if( (a) >= (l) ) {	\
			(a) = 0;		\
		}					\
	} while( 0 )

#define WITHIN(x,a,b)	( (x) >= (a) && (x) <= (b) )

#define ABS(a)			( (a) > 0 ? (a) : -(a) )
#define SQR(a)			( (a)*(a) )
#define VEC_P2(v)		( SQR((v)[0]) + SQR((v)[1]) + SQR((v)[2]) )
#define VEC_P1(v)		( ABS((v)[0]) + ABS((v)[1]) + ABS((v)[2]) )

#define CYCLE_DELAY(n)	\
	do {				\
		int j; for( j = 0; j < n; ++j ) asm("nop"); \
	} while( 0 )


void		unlock_io( void );
void		lock_io( void );

void		delay_ms( uint32_t );

unsigned int VirtToPhys( const void * );

void		uint2be_16( uint16_t, uint8_t * );
void		uint2be_32( uint32_t, uint8_t * );
uint16_t	be2uint_16( const uint8_t * );
uint32_t	be2uint_32( const uint8_t * );
uint32_t	be2uint( const uint8_t *, uint8_t );

void		uint2le_16( uint16_t, uint8_t * );
void		uint2le_32( uint32_t, uint8_t * );
uint16_t	le2uint_16( const uint8_t * );
uint32_t	le2uint_32( const uint8_t * );
uint32_t	le2uint( const uint8_t *, uint8_t );

#endif
