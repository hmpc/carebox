#ifndef _OSP_UART_H
#define	_OSP_UART_H

#define OSP_BAUD	4800
#define OSP_MAX_BUF	256
#define OSP_DMA_CHN	1
#define OSP_DMA_PRI	3

void	uart_config_io( void );
void	uart_init( void );
uint8_t	uart_read( void );
void	uart_write( uint8_t );
void	uart_flush( void );

#endif

