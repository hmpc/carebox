#ifndef _PIC_COM_H
#define _PIC_COM_H

#define PIC_COM_SDI_TRIS	TRISBbits.TRISB13
#define PIC_COM_SDO_TRIS	TRISAbits.TRISA2
#define PIC_COM_SCK_TRIS	TRISBbits.TRISB15

#define PIC_BUF_LEN			32


typedef enum {
	/* bidirectional commands */
	PIC_NO_CMD = 0x00,
	PIC_ACK,
	PIC_NACK,
	/* master-to-slave commands */
	PIC_READY,
	PIC_GET_CMD,
	PIC_NEW_ACC,
	PIC_EXCEP,
	/* slave-to-master requests */
	PIC_GET_UID,
	PIC_GET_COORD,
	PIC_GET_TIME,
	PIC_GET_SREC
} pic_cmd_t;


void	pic_com_config_io( void );
void	pic_com_init( void );
void	pic_com_start( void );
void	pic_com_suspend( void );

uint8_t	pic_com_send( uint8_t );
uint8_t	pic_com_get( void );

BOOL	pic_get_data( uint16_t, void * );
BOOL	pic_send_data( uint16_t, const void * );

uint8_t	pic_get_cmd( void );
void	pic_send_cmd( void );
void	pic_put_cmd( pic_cmd_t, unsigned int, const void * );

#endif
