#ifndef _FLASH_BUF_H
#define	_FLASH_BUF_H

#define FLASH_BUF_LEN	520
#define FLASH_JOURNAL_N	26

typedef enum {
	FLASH_J_NONE = 0, FLASH_J_WRITE, FLASH_J_ERASE
} flash_action_t;

typedef struct _flash_journal_t {
	flash_action_t	action;
	flash_addr_t	addr;
	uint32_t		count;
	void			*data;
} flash_journal_t;


void	flash_buf_write( flash_addr_t, uint32_t, const void * );
void	flash_buf_erase( flash_addr_t, uint32_t );
void	flash_buf_commit( unsigned int, volatile unsigned int * );

#endif	/* _FLASH_BUF_H */

