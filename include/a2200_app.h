#ifndef _A2200_H
#define	_A2200_H


/**
 * Supported devices
 */
#define A2200_A


/**
 * Hardware access
 */
#define A2200_TOGGLE	200		/* toggle pulse duration in ms */

void		gps_write( uint32_t, const uint8_t * );
uint32_t	gps_read( uint32_t, uint8_t * );


/**
 * Transport layer
 */
#define OSP_MAX_LENGTH	0x7FFF	/* maximum length of OSP frame payload */
#define OSP_CHKSUM_MASK	0x7FFF	/* OSP frame checksum bits */

#define OSP_TSTART_1	0xA0	/* OSP frame start pattern (2 bytes) */
#define OSP_TSTART_2	0xA2
#define OSP_TEND_1		0xB0	/* OSP frame end pattern (2 bytes) */
#define OSP_TEND_2		0xB3

/* protocols understood by the supported devices */
typedef enum {
	GPS_OSP,
	GPS_NMEA
} gps_protocol_t;

/* OSP checksum type */
typedef uint16_t osp_chksum_t;

osp_chksum_t	gps_osp_chksum( uint16_t, const uint8_t * );
BOOL			gps_osp_send( uint16_t, const uint8_t * );
BOOL			gps_osp_recv( uint16_t *, uint8_t * );


/**
 * Application layer
 */
#define NMEA_SWITCH_OSP		"$PSRF100,0,4800,8,1,0*0F\r\n"

#define OSP_MAX_MSG			256		/* maximum size of OSP message */
#define OSP_MAX_FIELD		8		/* maximum size of OSP message field */

#define OSP_IN_INIT			128		/* Initialize Data Source */
#define OSP_IN_NMEA			129		/* Switch To NMEA Protocol */
#define OSP_IN_POLL_SW		132		/* Poll Software Version */
#define OSP_IN_SET_SERIAL	134		/* Set Binary Serial Port */
#define OSP_IN_SET_PROTO	135		/* Set Protocol */
#define OSP_IN_STATIC_NAV	143		/* Static Navigation */
#define OSP_IN_POLL_NAV		152		/* Poll Navigation Parameters */
#define OSP_IN_SET_RATE		166		/* Set Message Rate */
#define OSP_IN_POLL_CMD		168		/* Poll Command Parameters */
#define OSP_IN_SOFT_OFF		205		/* Software Commanded Off */

#define OSP_OUT_ALL			0		/* all valid messages */
#define OSP_OUT_NAV_DATA	2		/* Measure Navigation Data Out */
#define OSP_OUT_TRACK_DATA	4		/* Measured Tracker Data Out */
#define OSP_OUT_SW_VERSION	6		/* Software Version String */
#define OSP_OUT_CLK_STATUS	7		/* Clock Status Data */
#define OSP_OUT_ERROR		10		/* Error ID Data */
#define OSP_OUT_CMD_ACK		11		/* Command Acknowledgement */
#define OSP_OUT_CMD_NACK	12		/* Command Negative Acknowledgement */
#define OSP_OUT_VISIBLE_SV	13		/* Visible List */
#define OSP_OUT_OK_TO_SEND	18		/* OkToSend */
#define OSP_OUT_NAV_PARAMS	19		/* Navigation Parameters */
#define OSP_OUT_GEO_DATA	41		/* Geodetic Navigation Data */
#define OSP_OUT_QUEUE_CMD	43		/* Queue Command Parameters */
#define OSP_OUT_1PPS_TIME	52		/* 1 PPS Time */

/* representation types for OSP message fields */
typedef uint32_t	osp_discrete_t;
typedef uint32_t	osp_unsigned_t;
typedef int32_t		osp_signed_t;
typedef float		osp_single_t;
typedef double		osp_double_t;

typedef struct {
	uint8_t size;

	enum {
		OSP_DISCRETE = 0, OSP_UNSIGNED, OSP_SIGNED, OSP_SINGLE, OSP_DOUBLE,
				OSP_DUMMY
	} type;

	union {
		osp_discrete_t	d;
		osp_unsigned_t	u;
		osp_signed_t	s;
		osp_single_t	sgl;
		osp_double_t	dbl;
	} value;
} osp_msg_field_t;

typedef uint8_t	osp_mid_t;

typedef struct {
	osp_mid_t		mid;
	uint8_t			n_fields;
	osp_msg_field_t	*fields;
} osp_msg_t;

#define OSP_MSG_CREATE( m, id, n, f )	do { \
	(m).mid = (id); \
	(m).n_fields = (n); \
	(m).fields = (f); \
} while(0)

#define OSP_MSG_FIELD( m, n, s, t )	do { \
	(m).fields[(n)].size = (s); \
	(m).fields[(n)].type = (t); \
} while(0)

#define OSP_MSG_GET( m, n, o ) \
	(o) = ( *( typeof(o) * )&(m).fields[(n)].value )

BOOL		gps_init( void );
BOOL		gps_on( gps_protocol_t );

BOOL		gps_f2a( const osp_msg_field_t *, uint8_t * );
BOOL		gps_a2f( uint8_t *, osp_msg_field_t * );
BOOL		gps_send( const osp_msg_t * );
BOOL		gps_recv( osp_msg_t * );


/**
 * GPS navigation
 */
#define GPS_NAV_FREQUENCY	1

#define GPS_TIME_VALID		0x01
#define GPS_TIME_UTC		0x02
#define GPS_TIME_CURRENT	0x04

typedef struct {
	uint8_t		hour;
	uint8_t		minute;
	uint16_t	second;
	uint8_t		day;
	uint8_t		month;
	uint16_t	year;
} gps_time_t;

typedef struct {
	uint16_t	valid;
	uint16_t	type;
	uint32_t	sats;

	uint16_t	week;
	uint32_t	tow;
	gps_time_t	time;

	int32_t		ecef_pos[3];
	int32_t		ecef_vel[3];

	int8_t		datum;
	int32_t		geo_pos[3];

	uint16_t	sog;
} gps_nav_t;

BOOL		gps_get_nav( gps_nav_t * );


/**
 * Represents an A2200-A unit. Includes configuration parameters and hardware
 * access functions.
 */
extern struct _A2200 {
	void	(*byte_writer)( uint8_t );
	uint8_t	(*byte_reader)( void );
	void	(*delay_ms)( uint32_t );
	void	(*set_onoff)( BOOL );

	gps_protocol_t	protocol;

	osp_msg_t		last_msg;
} a2200_a;


#endif

