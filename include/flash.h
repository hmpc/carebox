#ifndef _FLASH_H
#define	_FLASH_H

/**
 * Hardware definitions
 */
#define FLASH_CS		LATBbits.LATB4
#define FLASH_CS_TRIS	TRISBbits.TRISB4

// OUT OF THE PIC; SDO1
#define FLASH_SDI		LATAbits.LATA4
#define FLASH_SDI_TRIS	TRISAbits.TRISA4

// INTO THE PIC; SDI1
#define FLASH_SDO		LATBbits.LATB5
#define FLASH_SDO_TRIS	TRISBbits.TRISB5

#define FLASH_HW_EOW	PORTBbits.RB5

#define FLASH_SCK		LATBbits.LATB14
#define FLASH_SCK_TRIS	TRISBbits.TRISB14

#define FLASH_SPI_FREQ	10000000L

#define FLASH_JEDEC_ID	0xBF258C

/**
 * Flash status register fields
 */
#define FLASH_SR_BUSY	0x01
#define FLASH_SR_BP0	0x04
#define FLASH_SR_BP1	0x08

/**
 * Flash memory characteristics
 */
#define FLASH_SIZE			262144		// 256 Kbyte
#define FLASH_SECTOR_SIZE	4096		// 4 Kbyte
#define FLASH_BLOCK32_SIZE	32768		// 32 Kbyte
#define FLASH_BLOCK64_SIZE	65536		// 64 Kbyte
#define FLASH_SECTOR_NO		( FLASH_SIZE/FLASH_SECTOR_SIZE )
#define FLASH_BLOCK32_NO	( FLASH_SIZE/FLASH_BLOCK32_SIZE )
#define FLASH_BLOCK64_NO	( FLASH_SIZE/FLASH_BLOCK64_SIZE )

/**
 * Flash commands
 */
#define FLASH_WRSR			0x01
#define FLASH_BYTE_PROGRAM	0x02
#define FLASH_READ			0x03
#define FLASH_WRDI			0x04
#define FLASH_RDSR			0x05
#define FLASH_WREN			0x06
#define FLASH_EWSR			0x50
#define FLASH_4K_ERASE		0x20
#define FLASH_32K_ERASE		0x52
#define FLASH_64K_ERASE		0xD8
#define FLASH_CHIP_ERASE	0x60
#define FLASH_EBSY			0x70
#define FLASH_DBSY			0x80
#define FLASH_READID		0x9F
#define FLASH_AAI			0xAD

/**
 * Flash utilities
 */
typedef enum {
	FLASH_SECTOR = 0,
	FLASH_BLOCK_32,
	FLASH_BLOCK_64
} flash_block_t;

#define FLASH_SECTOR_MASK	( ~(FLASH_SECTOR_SIZE-1) )
#define FLASH_BLOCK32_MASK	( ~(FLASH_BLOCK32_SIZE-1) )
#define FLASH_BLOCK64_MASK	( ~(FLASH_BLOCK64_SIZE-1) )

#define FLASH_IN_SAME_SECTOR(a,b)	( !((a^b) & FLASH_SECTOR_MASK) )
#define FLASH_IN_SAME_BLOCK32(a,b)	( !((a^b) & FLASH_BLOCK32_MASK) )
#define FLASH_IN_SAME_BLOCK64(a,b)	( !((a^b) & FLASH_BLOCK64_MASK) )


typedef uint32_t flash_addr_t;


void		flash_config_io( void );
void		flash_init( void );

void		flash_clr_protect( void );

uint8_t		flash_read_status( void );
void		flash_write_status( uint8_t );
BOOL		flash_is_busy( void );
uint32_t	flash_get_device( void );

void		flash_read( flash_addr_t, uint32_t, void * );
void		flash_write_byte( flash_addr_t, uint8_t );
void		flash_raw_write( flash_addr_t, uint32_t, const void * );

void		flash_raw_erase( flash_addr_t, uint32_t, BOOL );
void		flash_erase_all( BOOL );

#endif
