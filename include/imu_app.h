#ifndef _IMU_APP_H
#define	_IMU_APP_H


#define IMU_LSM330DLC	/* 6-axis accelerometer and gyroscope */
#define IMU_MAG3110		/* 3-axis digital magnetometer */

typedef enum
{
	IMU_NONE = 0,
	IMU_ACCEL,
	IMU_GYRO,
	IMU_MAG
} imu_sensor_t;

#define N_SENSORS		3

/**
 * I2C slave addresses for on-board sensors
 */
#define IMU_ACCEL_ADDR	0b0011000
#define IMU_GYRO_ADDR	0b1101010
#define IMU_MAG_ADDR	0b0001110

extern const uint8_t imu_addr[ ];

#define ADDR_AUTO_INC	0x80


/**
 * Output data rates for accelerometer readings.
 *
 *		ACCEL_HZ_xx			xx Hz frequency (update time t_ut = 1/xx)
 */
typedef enum
{
	ACCEL_PWR_DOWN = 0,		/* power-down mode */
	ACCEL_HZ_1,
	ACCEL_HZ_10,
	ACCEL_HZ_25,
	ACCEL_HZ_50,
	ACCEL_HZ_100,
	ACCEL_HZ_200,
	ACCEL_HZ_400,
	ACCEL_HZ_1620,			/* low power mode only */
	ACCEL_HZ_1344			/* 5376 Hz in low power mode*/
} imu_accel_odr_t;

/**
 * Measurement ranges for accelerometer readings.
 *
 *      G_xx				+/- xx g maximum acceleration
 */
typedef enum {
	G_2 = 0,
	G_4,
	G_8,
	G_16
} imu_accel_range_t;

/**
 * Accelerometer CTRL1 control flags.
 */
#define ACCEL_LOW_PWR		0x08	/* low-power mode */

#define ACCEL_X_EN			0x01	/* enable X axis */
#define ACCEL_Y_EN			0x02	/* enable Y axis */
#define ACCEL_Z_EN			0x04	/* enable Z axis */
#define ACCEL_ZYX_EN		( ACCEL_X_EN | ACCEL_Y_EN | ACCEL_Z_EN )

/**
 * Accelerometer CTRL4 control flags.
 */
#define ACCEL_BLE			0x40	/* big endian data format */
#define ACCEL_HIGH_RES		0x08	/* high resolution output */

/**
 * Accelerometer CTRL5 control flags.
 */
#define ACCEL_BOOT			0x80	/* reboot memory content */
#define ACCEL_FIFO_EN		0x40	/* FIFO enable */

/**
 * Accelerometer status flags.
 */
#define ACCEL_ZYX_OR		0x80	/* X, Y and Z axis data overrun */
#define ACCEL_ZYX_DA		0x08	/* X, Y and Z axis new data available */


/**
 * Output data rates for gyroscope readings.
 *
 *		GYRO_HZ_xx			xx Hz frequency (update time t_ut = 1/xx)
 */
typedef enum {
	GYRO_HZ_95 = 0,
	GYRO_HZ_190,
	GYRO_HZ_380,
	GYRO_HZ_760,
} imu_gyro_odr_t;

/**
 * Measurement bandwidth for gyroscope readings. Indicated values are in Hz for
 * the rates defined in imu_gyro_odr_t.
 */
typedef enum {
	GYRO_BW_0 = 0,			/* 12.5, 12.5,  20,  30 */
	GYRO_BW_1,				/* 25,   25,    25,  35 */
	GYRO_BW_2,				/* 25,   50,    50,  50 */
	GYRO_BW_3				/* 25,   70,   100, 100 */
} imu_gyro_bw_t;

/**
 * Measurement ranges for gyroscope readings.
 *
 *      DPS_xxx				+/- xxx �/s maximum angular speed
 */
typedef enum {
	DPS_250 = 0,
	DPS_500,
	DPS_2000
} imu_gyro_range_t;

#define GYRO_ID				0xD4

/**
 * Gyroscope CTRL1 control flags.
 */
#define GYRO_POWER_DOWN		0x08	/* power-down mode disable */

#define GYRO_X_EN			0x02	/* enable X axis */
#define GYRO_Y_EN			0x01	/* enable Y axis */
#define GYRO_Z_EN			0x04	/* enable Z axis */
#define GYRO_ZYX_EN			( GYRO_X_EN | GYRO_Y_EN | GYRO_Z_EN )

/**
 * Gyroscope CTRL4 control flags.
 */
#define GYRO_BDU			0x80	/* block data update*/
#define GYRO_BLE			0x40	/* big endian data format */

/**
 * Gyroscope CTRL5 control flags.
 */
#define GYRO_BOOT			0x80	/* reboot memory content */
#define GYRO_FIFO_EN		0x40	/* FIFO enable */

/**
 * Gyroscope interrupt control.
 */
#define GYRO_INT_WAIT		0x80	/* enable wait on interrupt */

/**
 * Gyroscope status flags.
 */
#define GYRO_ZYX_OR			0x80	/* X, Y and Z axis data overrun */
#define GYRO_ZYX_DA			0x08	/* X, Y and Z axis new data available */


/**
 * Possible modes for FIFO buffers.
 */
typedef enum {
	FIFO_BYPASS = 0,		/* bypass mode */
	FIFO_FIFO,				/* FIFO mode */
	FIFO_STREAM,			/* stream mode */
	FIFO_STREAM_TRIGGER,	/* stream-to-FIFO mode */
	FIFO_BYPASS_TRIGGER		/* bypass-to-stream mode */
} imu_fifo_t;

#define FIFO_WTM			0x80	/* FIFO exceeded watermark level */
#define FIFO_OVRN			0x40	/* FIFO full (32 samples) */
#define FIFO_EMPTY			0x20	/* FIFO empty */

/**
 * Interrupt control flags for accelerometer and gyroscope.
 */
#define INT_XL				0x01	/* interrupt on X low */
#define INT_XH				0x02	/* interrupt on X high */
#define INT_YL				0x04	/* interrupt on Y low */
#define INT_YH				0x08	/* interrupt on Y high */
#define INT_ZL				0x10	/* interrupt on Z low */
#define INT_ZH				0x20	/* interrupt on Z high */

#define INT_ACTIVE			0x40	/* interrupt active */


/**
 * ADC sampling rates for magnetometer.
 *
 *		MAG_HZ_xxx			xxx Hz frequency
 */
typedef enum {
	MAG_HZ_1280 = 0,
	MAG_HZ_640,
	MAG_HZ_320,
	MAG_HZ_160,
	MAG_HZ_80
} imu_mag_adc_t;

/**
 * Over-sampling ratio for magnetometer measurements. Output data rate is given
 * by ADC rate / over-sampling ratio.
 */
typedef enum {
	MAG_OS_16 = 0,
	MAG_OS_32,
	MAG_OS_64,
	MAG_OS_128
} imu_mag_os_t;

#define MAG_ID				0xC4

/**
 * Magnetometer system mode flags.
 */
#define MAG_MOD_STANDBY		0x00	/* standby mode */
#define MAG_MOD_RAW			0x01	/* active mode, raw data */
#define MAG_MOD_NONRAW		0x02	/* active mode, user-corrected data */

/**
 * Magnetometer data status.
 */
#define MAG_ZYX_OW			0x80	/* X, Y and Z-axis data overwrite */
#define MAG_ZYX_DR			0x08	/* X, Y and Z-axis new data ready */

/**
 * Magnetometer CTRL1 control flags.
 */
#define MAG_ACTIVE			0x01	/* turn on periodic measurements */
#define MAG_TRIGGER			0x02	/* measure on trigger if on standby*/
#define MAG_FAST_READ		0x04	/* read only high byte of data */

/**
 * Magnetometer CTRL2 control flags.
 */
#define MAG_AUTO_RST		0x80	/* enable automatic sensor reset */
#define MAG_RAW				0x20	/* disable output data correction */
#define MAG_RST				0x10	/* initiate sensor reset cycle */


/**
 * Represents an IMU unit, composed of gyro, accelerometer and magnetometer.
 * Includes configuration parameters and hardware access functions.
 */
struct _CB_IMU
{
	uint8_t				accel_ctrl[3];
	imu_accel_odr_t		accel_odr;
	imu_accel_range_t	accel_range;
	int16_t				accel_data[3];

	uint8_t				gyro_ctrl[3];
	imu_gyro_odr_t		gyro_odr;
	imu_gyro_bw_t		gyro_bw;
	imu_gyro_range_t	gyro_range;
	int16_t				gyro_data[3];

	uint8_t				mag_ctrl[2];
	imu_mag_adc_t		mag_adc_rate;
	imu_mag_os_t		mag_os_rate;
	int16_t				mag_data[3];
};

extern struct _CB_IMU cb_imu;


/**
 * Generic IMU methods
 */
void	imu_init( void );
void	imu_sel( imu_sensor_t );
void	imu_reg_read( i2c_chan_t *, uint8_t, uint8_t, uint8_t * );
void	imu_reg_write( i2c_chan_t *, uint8_t, uint8_t );

/**
 * Accelerometer methods
 */
void	imu_accel_configure( void );
uint8_t	imu_accel_read_status( void );
BOOL	imu_accel_read( void );

/**
 * Gyroscope methods
 */
void	imu_gyro_configure( void );
uint8_t	imu_gyro_whoami( void );
int8_t	imu_gyro_read_temp( void );
uint8_t	imu_gyro_read_status( void );
BOOL	imu_gyro_read( void );

/**
 * Magnetometer methods
 */
void	imu_mag_configure( void );
uint8_t	imu_mag_whoami( void );
int8_t	imu_mag_read_temp( void );
uint8_t	imu_mag_read_status( void );
BOOL	imu_mag_read( void );


#endif
