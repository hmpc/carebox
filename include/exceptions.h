#ifndef _EXCEPTIONS_H
#define _EXCEPTIONS_H

/*
 * General errors
 */
typedef enum {
	ERR_NONE = 0,
	ERR_EXCEPTION,
	ERR_FLASH,
	ERR_IMU,
	ERR_GPS,
	ERR_COMM
} cb_error_t;

/*
 * General exceptions
 */
enum {
	EXCEP_IRQ = 0,			// interrupt
	EXCEP_AdEL = 4,			// address error exception (load or ifetch)
	EXCEP_AdES,				// address error exception (store)
	EXCEP_IBE,				// bus error (ifetch)
	EXCEP_DBE,				// bus error (load/store)
	EXCEP_Sys,				// syscall
	EXCEP_Bp,				// breakpoint
	EXCEP_RI,				// reserved instruction
	EXCEP_CpU,				// coprocessor unusable
	EXCEP_Overflow,			// arithmetic overflow
	EXCEP_Trap,				// trap (possible divide by zero)
	EXCEP_IS1 = 16,			// implementation specfic 1
	EXCEP_CEU,				// CorExtend Unuseable
	EXCEP_C2E				// coprocessor 2
};

/*
 * Flash errors
 */
enum {
	ERR_FLASH_ID = 1,
	ERR_FLASH_MAGIC,
	ERR_FLASH_ERASE,
	ERR_FLASH_J_OV,
	ERR_FLASH_J_WTF
};

/*
 * IMU errors
 */
enum {
	ERR_IMU_GYRO_ID = 1,
	ERR_IMU_MAG_ID,
	ERR_IMU_WRITE_SET,
	ERR_IMU_WRITE_PUT,
	ERR_IMU_READ_SET
};

/*
 * GPS errors
 */
enum {
	ERR_GPS_READ_NAV = 1
};

extern cb_error_t	last_err;
extern uint8_t		sub_err;

void	cb_throw( cb_error_t, uint8_t );

#endif
