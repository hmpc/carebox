#ifndef _CAREBOX_COMMON_H
#define	_CAREBOX_COMMON_H

#define FCY				40000000L
#define FPB				40000000L

#define MS_IN_S			1000
#define MS_CYCLES		(FPB/MS_IN_S)

#define FLASH_COMMIT_T			80
#define FLASH_COMMIT_TIMEOUT	80

#define F_SAMPLING		200
#define FS_PRE			20
#define FS_DUR			20
#define FS_POST			20

#define T_SAMPLING		(MS_IN_S/F_SAMPLING)
#define TS_PRE			(MS_IN_S/FS_PRE)
#define TS_DUR			(MS_IN_S/FS_DUR)
#define TS_POST			(MS_IN_S/FS_POST)

#define TIME_STORE_PRE	5
#define TIME_STORE_DUR	3
#define TIME_STORE_POST	2
#define TIME_ACCIDENT	( TIME_STORE_PRE + TIME_STORE_DUR + TIME_STORE_POST )

#define SYNC_THRESH		500

#define T_PIC_COMM		200

#define ACCEL_THRESHOLD	10000

#define GPS_ONOFF		(LATAbits.LATA3)

#define MAX_SREC_SEND	1

#endif

