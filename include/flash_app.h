#ifndef _FLASH_APP_H
#define	_FLASH_APP_H

typedef struct {
	flash_addr_t	off;
	uint32_t		size;
} flash_field_t;

typedef struct {
	uint8_t		count;
	uint8_t		first;
	uint8_t		last;
	uint8_t		dummy;
} flash_acctab_t;

typedef struct {
	uint32_t	tow;
	uint16_t	week;
	uint8_t		sync;

	uint8_t		fix;
	int32_t		lat;
	int32_t		lon;
	int32_t		alt;
	uint16_t	sog;

	uint16_t	acc_x;
	uint16_t	acc_y;
	uint16_t	acc_z;

	uint16_t	gyro_x;
	uint16_t	gyro_y;
	uint16_t	gyro_z;

	uint16_t	mag_x;
	uint16_t	mag_y;
	uint16_t	mag_z;
} flash_srec_t;

typedef uint32_t flash_uid_t;


#define FLASH_ACC_MAX_NO	1
#define FLASH_ACC_SREC_SIZE	( sizeof(flash_srec_t) )
#define FLASH_ACC_NO_PRE	( TIME_STORE_PRE*TS_PRE )
#define FLASH_ACC_NO_SREC	( TIME_STORE_PRE*FS_PRE + TIME_STORE_DUR*FS_DUR \
		+ TIME_STORE_POST*FS_POST )
#define FLASH_ACC_OCC		FLASH_BLOCK64_SIZE


#define FLASH_BASE			0

#define FLASH_CONST_OCC		FLASH_SECTOR_SIZE
#define FLASH_CONST_BASE	FLASH_BASE

#define FLASH_ACCTAB_OCC	FLASH_SECTOR_SIZE
#define FLASH_ACCTAB_BASE	( FLASH_CONST_BASE + FLASH_CONST_OCC )

#define FLASH_ACC_OCC		FLASH_BLOCK64_SIZE
#define FLASH_ACC_BASE		( FLASH_SIZE - FLASH_ACC_MAX_NO*FLASH_ACC_OCC )

#define FLASH_PAD_BASE		( FLASH_ACCTAB_BASE + FLASH_ACCTAB_OCC )
#define FLASH_PAD_OCC		( FLASH_ACC_BASE - FLASH_PAD_BASE )

#define FLASH_PAD_SREC_OFF	( FLASH_SECTOR_SIZE % FLASH_ACC_SREC_SIZE )
#define FLASH_PAD_ALIGN(a)	( ( (a) & FLASH_SECTOR_MASK ) + FLASH_PAD_SREC_OFF )

#define FLASH_MAGIC			0xCB73

#define FLASH_ACC_I2A(i)	( FLASH_ACC_BASE + (i)*FLASH_ACC_OCC )
#define FLASH_ACC_REC2A(i,n)	\
	( FLASH_ACC_I2A(i) + (n)*FLASH_ACC_SREC_SIZE )


void			flash_rec_init( void );

uint16_t		flash_get_magic( void );
void			flash_set_magic( uint16_t );
flash_uid_t		flash_get_uid( void );
void			flash_set_uid( flash_uid_t );

void			flash_read_srec( flash_addr_t, flash_srec_t * );
void			flash_put_srec( const flash_srec_t * );
void			flash_get_srec( unsigned int, unsigned int, flash_srec_t * );

flash_addr_t	flash_start_acc( void );
uint8_t			flash_end_acc( void );
uint8_t			flash_get_last_acc( void );

#endif	/* FLASH_APP_H */

