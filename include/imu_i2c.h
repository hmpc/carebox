#ifndef _IMU_I2C_H
#define	_IMU_I2C_H


#define I2C_BAUD		100000L
#define I2C_NO_ADDR		0xFF

/**
 * I2C channel state
 *
 *		IDLE			channel is idle
 *		WRITING			master is writing a slave register
 *		READING			master is reading a slave register
 */
typedef enum
{
	I2C_IDLE = 0,
	I2C_WRITING,
	I2C_READING
} i2c_state_t;


/**
 * Represents an active I2C channel to read and write registers from and to a
 * slave device.
 *
 *		channel			I2C hardware peripheral
 *		state			current state of the channel
 *		slave_addr		address of the slave device currently active
 */
typedef struct _I2C_CHANNEL
{
	I2C_MODULE	channel;
	i2c_state_t	state;
	uint8_t		slave_addr;
} i2c_chan_t;


void	i2c_init( i2c_chan_t *, I2C_MODULE );

BOOL	i2c_put( I2C_MODULE, uint8_t );
uint8_t	i2c_get( I2C_MODULE );

void	i2c_idle( i2c_chan_t * );
void	i2c_select( i2c_chan_t *, uint8_t );

BOOL	i2c_set( i2c_chan_t *, BOOL );
BOOL	i2c_write( i2c_chan_t *, uint8_t, BOOL );
uint8_t	i2c_read( i2c_chan_t *, BOOL );


#endif
