#include <inttypes.h>
#include <GenericTypeDefs.h>
#include <stdio.h>
#include <stdlib.h>
#include <plib.h>

#include "carebox_common.h"
#include "util.h"
#include "exceptions.h"
#include "flash.h"
#include "flash_buf.h"
#include "flash_app.h"
#include "a2200_uart.h"
#include "a2200_app.h"
#include "imu_i2c.h"
#include "imu_app.h"
#include "pic_com.h"


#pragma config IOL1WAY = OFF
#pragma config FPLLODIV = DIV_2
#pragma config FPLLMUL = MUL_20
#pragma config FPLLIDIV = DIV_2
#pragma config FWDTEN = OFF
#pragma config FPBDIV = DIV_1
#pragma config WDTPS = PS1
#pragma config FCKSM = CSECME
#pragma config OSCIOFNC = OFF
#pragma config POSCMOD = OFF
#pragma config IESO = ON
#pragma config FSOSCEN = OFF
#pragma config FNOSC = FRCPLL
#pragma config CP = OFF
#pragma config BWP = OFF
#pragma config PWP = OFF
#pragma config ICESEL = ICS_PGx1
#pragma config JTAGEN = OFF
#pragma config DEBUG = OFF


/**
 * System variables
 */
flash_uid_t		cb_uid;


/**
 * Shared variables
 */
uint32_t		cur_time = 0;
uint8_t			cur_sync = 0;

gps_nav_t		cur_nav;
gps_nav_t		next_nav;

BOOL			accident = FALSE;


/**
 *
 */
void fill_srec( flash_srec_t *rec ) {
	rec->week	= cur_nav.week;
	rec->sync	= cur_sync;
	rec->tow	= cur_time;

	rec->fix	= cur_nav.valid;
	rec->lat	= cur_nav.geo_pos[0];
	rec->lon	= cur_nav.geo_pos[1];
	rec->alt	= cur_nav.geo_pos[2];
	rec->sog	= cur_nav.sog;

	rec->acc_x	= cb_imu.accel_data[0];
	rec->acc_y	= cb_imu.accel_data[1];
	rec->acc_z	= cb_imu.accel_data[2];

	rec->gyro_x	= cb_imu.gyro_data[0];
	rec->gyro_y	= cb_imu.gyro_data[1];
	rec->gyro_z	= cb_imu.gyro_data[2];

	rec->mag_x	= cb_imu.mag_data[0];
	rec->mag_y	= cb_imu.mag_data[1];
	rec->mag_z	= cb_imu.mag_data[2];
}

inline
BOOL is_accident( void ) {
	return ( VEC_P1( cb_imu.accel_data ) > ACCEL_THRESHOLD );
}

void __ISR( _TIMER_1_VECTOR, ipl3 ) int_sensing( void ) {
	const unsigned int nl_dur = F_SAMPLING * TIME_STORE_DUR;
	const unsigned int nl_post = nl_dur + F_SAMPLING * TIME_STORE_POST;

	static unsigned int nl_store = F_SAMPLING / FS_PRE;
	static unsigned int ts = TS_PRE;

	static flash_addr_t pre_addr;
	static unsigned int l_pre = 0;

	static unsigned int l_store = 0;
	static unsigned int l_acc = 0;

	flash_srec_t	sensor_data;

	IFS0bits.T1IF = 0;

	/* read sensor data every sampling period */
	imu_accel_read( );
	imu_gyro_read( );
//	imu_mag_read( );

	/* update time and nav solution */
	if( cur_nav.tow != next_nav.tow ) {
		cur_nav = next_nav;
	}
	cur_time += ts;

	if( accident ) {
		++l_acc;
		if( l_acc >= nl_post ) {
			/* end of accident recording time */
			ts = TS_PRE;
			nl_store = F_SAMPLING / FS_PRE;
			accident = FALSE;

			uint8_t acc = flash_end_acc( );
			pic_put_cmd( PIC_NEW_ACC, 1, &acc );
		} else if( l_acc >= nl_dur ) {
			/* end of initial phase of accident */
			ts = TS_POST;
			nl_store = F_SAMPLING / FS_POST;
		}
	} else if( is_accident( ) ) {
		/* accident is occurring */
		pre_addr = flash_start_acc( );

		accident = TRUE;
		ts = TS_DUR;
		nl_store = F_SAMPLING / FS_DUR;
		l_acc = 0;
		l_pre = 0;
	}

	/* store data every nl_store cycles */
	++l_store;
	if( l_store >= nl_store ) {
		fill_srec( &sensor_data );
		flash_put_srec( &sensor_data );
		l_store = 0;

		/* copy pre-crash data (FLASH_ACC_NO_PRE blocks) to accident storage */
		if( accident && l_pre < FLASH_ACC_NO_PRE ) {
			flash_addr_t nr;
			flash_srec_t srec;

			nr = pre_addr - FLASH_ACC_SREC_SIZE;
			if( nr < FLASH_PAD_BASE ) {
				nr += FLASH_PAD_OCC;
			}
			if( !FLASH_IN_SAME_SECTOR( nr, pre_addr ) ) {
				nr = ( nr & FLASH_SECTOR_MASK ) + FLASH_SECTOR_SIZE
						- FLASH_ACC_SREC_SIZE;
			}

			flash_read_srec( nr, &srec );
			flash_put_srec( &srec );

			pre_addr = nr;
			++l_pre;
		}
	}
}


/**
 * Flash journaling routine
 */
void __ISR( _TIMER_5_VECTOR, ipl2 ) int_flash_journal( void ) {
	const unsigned int timeout = FLASH_COMMIT_TIMEOUT*MS_CYCLES/256;
	IFS0bits.T5IF = 0;
	flash_buf_commit( timeout, &TMR5 );
}


/**
 * PIC communication
 */
void pic_req_uid( uint8_t code ) {
	pic_send_data( sizeof(cb_uid), &cb_uid );
}

void pic_req_coord( uint8_t code ) {
	pic_send_data( 8, cur_nav.geo_pos );
}

void pic_req_time( uint8_t code ) {
	pic_send_data( sizeof(cur_time), &cur_time );
}

void pic_req_srec( uint8_t code ) {
	const uint8_t ack = PIC_ACK;
	unsigned int cur_blk = 0;
	flash_srec_t srec;

	/* disable interrupts to focus on accident transmission */
	asm("di");

	/* transmit srec blocks */
	while( 1 ) {
		pic_send_data( 1, &ack );
		flash_get_srec( 0, cur_blk, &srec );
		pic_send_data( sizeof(srec), &srec );

		++cur_blk;
		if( cur_blk < FLASH_ACC_NO_SREC ) {
			while( pic_get_cmd( ) != PIC_GET_SREC );
		} else {
			break;
		}
	}

	/* reenable interrupts */
	asm("ei");
}

typedef void (*pic_req_t)( uint8_t );
void __ISR( _TIMER_4_VECTOR, ipl1 ) int_comm( void ) {
	const pic_req_t reqs[ ] = {
		[PIC_NO_CMD] = NULL,
		[PIC_GET_UID] = pic_req_uid,
		[PIC_GET_COORD] = pic_req_coord,
		[PIC_GET_TIME] = pic_req_time,
		[PIC_GET_SREC] = pic_req_srec,
	};
	const unsigned int n_reqs = sizeof(reqs) / sizeof(pic_req_t);
	uint8_t code;

	IFS0bits.T4IF = 0;

	/* send pending command */
	pic_send_cmd( );

	/* answer slave requests */
	code = pic_get_cmd( );
	/*
	if( code < n_reqs && reqs[code] != NULL ) {
//		pic_com_send( PIC_ACK );
		(*reqs[code])( code );
	} else {
//		pic_com_send( PIC_NACK );
	}
	*/
	switch( code ) {
		case PIC_NO_CMD:
			break;

		case PIC_GET_UID:
			pic_req_uid( code );
			break;

		case PIC_GET_COORD:
			pic_req_coord( code );
			break;

		case PIC_GET_TIME:
			pic_req_time( code );
			break;

		case PIC_GET_SREC:
			pic_req_srec( code );
			break;

		default:
			break;
	}
}


/**
 * GPS helper functions
 */
inline
void gps_onoff( BOOL on ) {
	GPS_ONOFF = on;
}


/**
 * Main function
 */
int main( void )
{
    SYSTEMConfigPerformance( FPB );
	INTEnableSystemMultiVectoredInt( );

    /*
	 * I/O initialisation
	 */
	TRISAbits.TRISA3 = 0;
	ANSELBbits.ANSB2 = 0;
	ANSELBbits.ANSB3 = 0;

	unlock_io( );
	flash_config_io( );
	uart_config_io( );
	pic_com_config_io( );
	lock_io( );


	/*
	 * Flash initialisation and check
	 */
	flash_rec_init( );
	if( flash_get_magic( ) != FLASH_MAGIC ) {
		cb_throw( ERR_FLASH, ERR_FLASH_MAGIC );
	}

	ConfigIntTimer5( T5_INT_ON | T5_INT_PRIOR_2 );
	OpenTimer5( T5_ON | T5_PS_1_256, FLASH_COMMIT_T*MS_CYCLES/256 );


	/*
	 * IMU initialisation
     */
	imu_init( );

	cb_imu.accel_ctrl[0] = ACCEL_ZYX_EN;
	cb_imu.accel_odr = ACCEL_HZ_200;
	cb_imu.accel_range = G_8;

	imu_accel_configure( );

	if( imu_gyro_whoami( ) != GYRO_ID ) {
		cb_throw( ERR_IMU, ERR_IMU_GYRO_ID );
	}

	cb_imu.gyro_ctrl[0] = GYRO_ZYX_EN;
	cb_imu.gyro_odr = GYRO_HZ_380;
	cb_imu.gyro_range = DPS_250;
	cb_imu.gyro_bw = GYRO_BW_0;

	imu_gyro_configure( );

#if 0
	cb_imu.mag_ctrl[0] = MAG_ACTIVE;
	cb_imu.mag_ctrl[1] = MAG_RAW;
	cb_imu.mag_adc_rate = MAG_HZ_640;
	cb_imu.mag_os_rate = MAG_OS_64;

	imu_mag_configure( );
	if( imu_mag_whoami( ) != MAG_ID ) {
		cb_throw( ERR_IMU, ERR_IMU_MAG_ID );
	}
#endif


	/*
	 * GPS initialisation
     */
	uart_init( );
	gps_init( );

	a2200_a.byte_reader = uart_read;
	a2200_a.byte_writer = uart_write;
	a2200_a.delay_ms = delay_ms;
	a2200_a.set_onoff = gps_onoff;

	gps_on( GPS_OSP );


	/*
	 * PIC communication setup
     */
	pic_com_init( );

	ConfigIntTimer4( T4_INT_ON | T4_INT_PRIOR_1 );
	OpenTimer4( T4_ON | T4_PS_1_256, T_PIC_COMM*MS_CYCLES/256 );


	/*
	 * Finish initialisations and set up communications
	 */
	cb_uid = flash_get_uid( );


	/*
	 * Main loop
	 */

	/* timer and interrupt for sensing loop */
	ConfigIntTimer1( T1_INT_ON | T1_INT_PRIOR_3 );
	OpenTimer1( T1_OFF | T1_PS_1_64, T_SAMPLING*MS_CYCLES/64 );

    while( 1 ) {
		if( gps_get_nav( &next_nav ) == FALSE ) {
			cb_throw( ERR_GPS, ERR_GPS_READ_NAV );
		}

		if( next_nav.week == 0x6E2 ) {
			asm("nop");
		}

		/* resync time */
		int delta = cur_time - next_nav.tow;
		if( delta > SYNC_THRESH || delta < -SYNC_THRESH ) {
			cur_time = next_nav.tow;
			++cur_sync;
		}

		T1CONbits.ON = 1;
		delay_ms( 900 );
	}

    return EXIT_SUCCESS;
}


/**
 * General exception handling
 */
static unsigned int _excep_code;
static unsigned int _excep_addr;
static unsigned int _excep_badaddr;
void _general_exception_handler( void ) {
	asm volatile( "mfc0 %0,$13" : "=r" (_excep_code) );
	asm volatile( "mfc0 %0,$14" : "=r" (_excep_addr) );

	_excep_code = (_excep_code & 0x0000007C) >> 2;
	if( _excep_code == EXCEP_AdEL || _excep_code == EXCEP_AdES ) {
		asm volatile( "mfc0 %0,$8" : "=r" (_excep_badaddr) );
	}
	cb_throw( ERR_EXCEPTION, _excep_code );
	asm("eret");
}
