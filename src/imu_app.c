#include <inttypes.h>
#include <GenericTypeDefs.h>
#include <stdlib.h>
#include <plib.h>

#include "carebox_common.h"
#include "exceptions.h"
#include "imu_i2c.h"
#include "imu_app.h"


const uint8_t imu_addr[ ] = {
	[IMU_NONE]	= I2C_NO_ADDR,
	[IMU_ACCEL]	= IMU_ACCEL_ADDR,
	[IMU_GYRO]	= IMU_GYRO_ADDR,
	[IMU_MAG]	= IMU_MAG_ADDR
};

i2c_chan_t lsm_chan, mag_chan;

struct _CB_IMU cb_imu;


void imu_init( void ) {
	i2c_init( &mag_chan, I2C1 );
	i2c_init( &lsm_chan, I2C2 );

	cb_imu.accel_odr = ACCEL_PWR_DOWN;
	cb_imu.accel_range = G_2;
}

inline
void imu_sel( imu_sensor_t device ) {
	i2c_select( ( device == IMU_MAG ) ? &mag_chan : &lsm_chan,
			imu_addr[device] );
}

void imu_reg_read( i2c_chan_t *chan, uint8_t start, uint8_t n, uint8_t *data ) {
    int i;

	if( n > 1 ) {
		start |= ADDR_AUTO_INC;
	}
    i2c_write( chan, start, FALSE );
    for( i = 1; i < n; ++i ) {
        *data++ = i2c_read( chan, FALSE );
    }
	*data = i2c_read( chan, TRUE );
}

void imu_reg_write( i2c_chan_t *chan, uint8_t reg, uint8_t data ) {
	i2c_write( chan, reg, FALSE );
	i2c_write( chan, data, TRUE );
}


void imu_accel_configure( void ) {
	const uint8_t ctrl1_reg = 0x20, ctrl4_reg = 0x23, ctrl5_reg = 0x24;

	imu_sel( IMU_ACCEL );

	imu_reg_write( &lsm_chan, ctrl1_reg, (cb_imu.accel_odr << 4) |
			cb_imu.accel_ctrl[0] );
	imu_reg_write( &lsm_chan, ctrl4_reg, (cb_imu.accel_range << 4) |
			cb_imu.accel_ctrl[1] );
	imu_reg_write( &lsm_chan, ctrl5_reg, cb_imu.accel_ctrl[2] );
}

uint8_t imu_accel_read_status( void ) {
	const uint8_t status_reg = 0x27;
	uint8_t status = 0;

	imu_sel( IMU_ACCEL );
	imu_reg_read( &lsm_chan, status_reg, 1, &status );

	return status;
}

BOOL imu_accel_read( void ) {
	const uint8_t data_reg = 0x27;
	const uint8_t n_bytes = 7;

	uint8_t data[8] = { 0 };

	imu_sel( IMU_ACCEL );
	imu_reg_read( &lsm_chan, data_reg, n_bytes, data );
	cb_imu.accel_data[0] = le2uint_16( &data[1] );
	cb_imu.accel_data[1] = le2uint_16( &data[3] );
	cb_imu.accel_data[2] = le2uint_16( &data[5] );

	return ( data[0] & ACCEL_ZYX_DA );
}


void imu_gyro_configure( void ) {
	const uint8_t ctrl1_reg = 0x20, ctrl4_reg = 0x23, ctrl5_reg = 0x24;

	imu_sel( IMU_GYRO );

	imu_reg_write( &lsm_chan, ctrl1_reg, (cb_imu.gyro_odr << 6) |
			(cb_imu.gyro_bw << 4) | cb_imu.gyro_ctrl[0] );
	imu_reg_write( &lsm_chan, ctrl4_reg, (cb_imu.gyro_range << 4) |
			cb_imu.gyro_ctrl[1] );
	imu_reg_write( &lsm_chan, ctrl5_reg, cb_imu.gyro_ctrl[2] );
}

uint8_t imu_gyro_whoami( void ) {
	const uint8_t whoami_reg = 0x0F;
	uint8_t whoami = 0x00;

	imu_sel( IMU_GYRO );
	imu_reg_read( &lsm_chan, whoami_reg, 1, &whoami );
	return whoami;
}

int8_t imu_gyro_read_temp( void ) {
	const uint8_t temp_reg = 0x26;
	int8_t temp = 0x00;

	imu_sel( IMU_GYRO );
	imu_reg_read( &lsm_chan, temp_reg, 1, &temp );
	return temp;
}

uint8_t imu_gyro_read_status( void ) {
	const uint8_t status_reg = 0x27;
	uint8_t status = 0x00;

	imu_sel( IMU_GYRO );
	imu_reg_read( &lsm_chan, status_reg, 1, &status );
	return status;
}

BOOL imu_gyro_read( void ) {
	const uint8_t data_reg = 0x27;
	const uint8_t n_bytes = 7;

	uint8_t data[8] = { 0 };

	imu_sel( IMU_GYRO );
	imu_reg_read( &lsm_chan, data_reg, n_bytes, data );
	cb_imu.gyro_data[0] = le2uint_16( &data[1] );
	cb_imu.gyro_data[1] = le2uint_16( &data[3] );
	cb_imu.gyro_data[2] = le2uint_16( &data[5] );

	return ( data[0] & GYRO_ZYX_DA );
}


void imu_mag_configure( void ) {
	const uint8_t ctrl1_reg = 0x10;
	const uint8_t ctrl2_reg = 0x11;

	imu_sel( IMU_MAG );
	imu_reg_write( &mag_chan, ctrl1_reg, ( cb_imu.mag_adc_rate << 5 ) |
			( cb_imu.mag_os_rate << 3 ) | cb_imu.mag_ctrl[0] );
	imu_reg_write( &mag_chan, ctrl2_reg, cb_imu.mag_ctrl[1] );
}

uint8_t imu_mag_whoami( void ) {
	const uint8_t whoami_reg = 0x07;
	uint8_t whoami = 0x00;

	imu_sel( IMU_MAG );
	imu_reg_read( &mag_chan, whoami_reg, 1, &whoami );
	return whoami;
}

int8_t imu_mag_read_temp( void ) {
	const uint8_t temp_reg = 0x0F;
	int8_t temp = 0x00;

	imu_sel( IMU_MAG );
	imu_reg_read( &mag_chan, temp_reg, 1, &temp );
	return temp;
}

uint8_t imu_mag_read_status( void ) {
	const uint8_t status_reg = 0x00;
	uint8_t status = 0x00;

	imu_sel( IMU_MAG );
	imu_reg_read( &mag_chan, status_reg, 1, &status );
	return status;
}

BOOL imu_mag_read( void ) {
	const uint8_t start_reg = 0x00;
	const uint8_t n_axis = 3;
	const uint8_t n_bytes = 7;

	uint8_t i;
	uint8_t data[8];

	imu_sel( IMU_MAG );

	if( cb_imu.mag_ctrl[0] & MAG_FAST_READ ) {
		imu_reg_read( &mag_chan, start_reg, n_axis + 1, data );
		for( i = 1; i <= n_axis; ++i ) {
			cb_imu.mag_data[i-1] = data[i] << 8;
		}
	} else {
		imu_reg_read( &mag_chan, start_reg, n_bytes, data );
		for( i = 1; i <= n_axis; ++i ) {
			cb_imu.mag_data[i-1] = ( data[ (i<<1)-1 ] << 8 ) | data[ (i<<1) ];
		}
	}

	return ( data[0] & MAG_ZYX_DR ) ? TRUE : FALSE;
}
