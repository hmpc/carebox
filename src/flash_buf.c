#include <string.h>
#include <inttypes.h>
#include <GenericTypeDefs.h>

#include "carebox_common.h"
#include "util.h"
#include "exceptions.h"
#include "flash.h"
#include "flash_buf.h"

#define ATOM_OP(op)		\
	do {				\
		asm("di");		\
		op;				\
		asm("ei");		\
	} while( 0 )
#define ATOM_DEC(v)		ATOM_OP( --(v) )
#define ATOM_INC(v)		ATOM_OP( ++(v) )

static uint8_t		buf[ FLASH_BUF_LEN ];
static unsigned int cur_buf = 0;

static flash_journal_t	journal[ FLASH_JOURNAL_N ];
static unsigned int		j_count = 0;
unsigned int		j_first = 0, j_last = 0;


void flash_buf_commit( unsigned int timeout, volatile unsigned int *timer ) {
	flash_journal_t *e;

	while( j_first != j_last && *timer < timeout ) {
		if( j_first >= FLASH_JOURNAL_N ) {
			/* this should never ever happen, but it does */
			cb_throw( ERR_FLASH, ERR_FLASH_J_WTF );
		}
		e = &journal[j_first];
		asm("nop");
		switch( e->action ) {
			case FLASH_J_WRITE:
				flash_raw_write( e->addr, e->count, e->data );
				break;

			case FLASH_J_ERASE:
				flash_raw_erase( e->addr, e->count, TRUE );
				break;

			default:
				break;
		}
		ATOM_DEC( j_count );
		//INC_WRAP( j_first, FLASH_JOURNAL_N );
		if( ++j_first >= FLASH_JOURNAL_N ) j_first = 0;
	}
}

static
void flash_buf_add( flash_action_t action, flash_addr_t addr, uint32_t count,
		void *data ) {
	flash_journal_t *entry;

	if( j_last >= FLASH_JOURNAL_N ) {
		/* this should never ever happen, but it does */
		cb_throw( ERR_FLASH, ERR_FLASH_J_WTF );
	}

	entry = &journal[j_last];
	//INC_WRAP( j_last, FLASH_JOURNAL_N );
	if( ++j_last >= FLASH_JOURNAL_N ) j_last = 0;

	ATOM_INC( j_count );
	//if( j_count > FLASH_JOURNAL_N ) {
	if( j_last == j_first ) {
		cb_throw( ERR_FLASH, ERR_FLASH_J_OV );
	}

	entry->action = action;
	entry->addr = addr;
	entry->count = count;
	entry->data = data;
}

void flash_buf_write( flash_addr_t addr, uint32_t count, const void *data ) {
	if( cur_buf + count > FLASH_BUF_LEN ) {
		cur_buf = 0;
	}
	memcpy( &buf[cur_buf], data, count );
	flash_buf_add( FLASH_J_WRITE, addr, count, &buf[cur_buf] );
	cur_buf += count;
}

void flash_buf_erase( flash_addr_t addr, uint32_t count ) {
	flash_buf_add( FLASH_J_ERASE, addr, count, NULL );
}
