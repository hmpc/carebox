#include <inttypes.h>
#include <GenericTypeDefs.h>
#include <plib.h>

#include "carebox_common.h"
#include "util.h"
#include "exceptions.h"
#include "imu_i2c.h"


void i2c_init( i2c_chan_t *chan, I2C_MODULE mod ) {
	chan->channel = mod;
    chan->state = I2C_IDLE;

	I2CSetFrequency( mod, FPB, I2C_BAUD );
	I2CEnable( mod, TRUE );
}

void i2c_idle( i2c_chan_t *chan ) {
	const I2C_MODULE mod = chan->channel;

	switch( chan->state ) {
		case I2C_READING:
			I2CAcknowledgeByte( mod, FALSE );

		case I2C_WRITING:
			I2CStop( mod );
			break;

		default:
			break;
	}

	switch( mod ) {
		case I2C1:
			IdleI2C1( );
			break;

		case I2C2:
			IdleI2C2( );
			break;

		default:
			break;
	}

	chan->state = I2C_IDLE;
}

BOOL i2c_put( I2C_MODULE mod, uint8_t byte ) {
	while( !I2CTransmitterIsReady( mod ) );
	while( I2CSendByte( mod, byte ) != I2C_SUCCESS ) {
		I2CClearStatus( mod, I2C_TRANSMITTER_OVERFLOW | I2C_ARBITRATION_LOSS );
	}
	while( !I2CTransmissionHasCompleted( mod ) );

	return I2CByteWasAcknowledged( mod );
}

uint8_t i2c_get( I2C_MODULE mod ) {
	I2CReceiverEnable( mod, TRUE );
	while( !I2CReceivedDataIsAvailable( mod ) );
	return I2CGetByte( mod );
}

void i2c_select( i2c_chan_t *chan, uint8_t addr ) {
	if( chan->slave_addr != addr ) {
		i2c_idle( chan );
		chan->slave_addr = addr;
		i2c_idle( chan );
	}
}

inline
BOOL i2c_set( i2c_chan_t *chan, BOOL rw ) {
	return i2c_put( chan->channel, ( chan->slave_addr << 1 ) |
			( rw ? I2C_WRITE : I2C_READ ) );
}

BOOL i2c_write( i2c_chan_t *chan, uint8_t data, BOOL stop ) {
	const I2C_MODULE mod = chan->channel;
	BOOL done = FALSE;

	while( !done ) {
		switch( chan->state ) {
			case I2C_IDLE:
				I2CStart( mod );
				if( !i2c_set( chan, TRUE ) ) {
					cb_throw( ERR_IMU, ERR_IMU_WRITE_SET );
				}
				chan->state = I2C_WRITING;
				break;

			case I2C_WRITING:
				if( !i2c_put( mod, data ) ) {
					cb_throw( ERR_IMU, ERR_IMU_WRITE_PUT );
				}

				if( stop ) {
					i2c_idle( chan );
				}

				done = TRUE;
				break;

			default:
				i2c_idle( chan );
				break;
		}
	}

	return TRUE;
}

uint8_t i2c_read( i2c_chan_t *chan, BOOL stop ) {
	const I2C_MODULE mod = chan->channel;
	BOOL done = FALSE;
	uint8_t data = 0;

	while( !done ) {
		switch( chan->state ) {
			case I2C_IDLE:
				I2CStart( mod );
				if( !i2c_set( chan, FALSE ) ) {
					cb_throw( ERR_IMU, ERR_IMU_READ_SET );
				}
				chan->state = I2C_READING;
				break;

			case I2C_WRITING:
				I2CRepeatStart( mod );
				if( !i2c_set( chan, FALSE ) ) {
					cb_throw( ERR_IMU, ERR_IMU_READ_SET );
				}
				chan->state = I2C_READING;
				break;

			case I2C_READING:
				data = i2c_get( mod );

				if( stop ) {
					i2c_idle( chan );
				} else {
					I2CAcknowledgeByte( mod, TRUE );
					while( !I2CAcknowledgeHasCompleted( mod ) );
				}

				done = TRUE;
				break;

			default:
				i2c_idle( chan );
				break;
		}
	}

	return data;
}
