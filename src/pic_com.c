#include <plib.h>
#include <inttypes.h>

#include "pic_com.h"


static pic_cmd_t	pic_next = PIC_NO_CMD;
static unsigned int	pic_len = 0;
static uint8_t		pic_data[ PIC_BUF_LEN ] = { 0 };


void pic_com_config_io( void ) {
	RPA2R = 0b0100;
	SDI2R = 0b0011;
}

void pic_com_init( void ) {
	// Pin directions
	PIC_COM_SDI_TRIS = 1;
	PIC_COM_SDO_TRIS = 0;
	PIC_COM_SCK_TRIS = 0;
	TRISBCLR = 1 << 15;
	ANSELB = 0;

	// SPI channel config
	// Interrupt on RX
	SPI2CON = 0;

	IEC1bits.SPI2RXIE = 0;
	IEC1bits.SPI2TXIE = 0;

	IPC9bits.SPI2IP = 6;
	IPC9bits.SPI2IS = 3;

	SPI2BRG = 15;
	SPI2STATbits.SPIROV = 0;

	SPI2CONbits.MSTEN = 1;
	SPI2CONbits.ON = 1;
}


uint8_t pic_com_send( uint8_t byte ) {
	SpiChnPutC( 2, byte );
	return SpiChnGetC( 2 );
}

uint8_t pic_com_get( void ) {
	while ( !SPI2STATbits.SPITBE );
	//SPI2BUF = 0x00;
	SPI2BUF = '!';
	while ( !SPI2STATbits.SPIRBF );
	return SPI2BUF;
//	SpiChnPutC( 2, '!' );
//	return SpiChnGetC( 2 );
}


BOOL pic_send_data( uint16_t n, const void *data ) {
	while( n-- ) {
		pic_com_send( *( uint8_t * )data++ );
	}
	return TRUE;
}

BOOL pic_get_data( uint16_t n, void *data ) {
	while( n-- ) {
		*( uint8_t * )data++ = pic_com_get( );
	}
	return TRUE;
}


inline
uint8_t pic_get_cmd( void ) {
	return pic_com_send( PIC_GET_CMD );
}

void pic_send_cmd( void ) {
	if( pic_next == PIC_NO_CMD ) {
		return;
	}
	pic_com_send( pic_next );
	pic_send_data( pic_len, pic_data );
	pic_next = PIC_NO_CMD;
}

void pic_put_cmd( pic_cmd_t cmd, unsigned int n, const void *data ) {
	pic_next = cmd;
	pic_len = n;
	memcpy( pic_data, data, n );
}
