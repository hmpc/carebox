#include <inttypes.h>
#include <GenericTypeDefs.h>
#include <xc.h>
#include <plib.h>

#include "carebox_common.h"
#include "util.h"


inline
void unlock_io( void ) {
	SYSKEY = 0x0;
	SYSKEY = 0xAA996655;
	SYSKEY = 0x556699AA;
	CFGCONbits.IOLOCK = 0;
}

inline
void lock_io( void ) {
	CFGCONbits.IOLOCK = 1;
	SYSKEY = 0x33333333;
}

inline
unsigned int VirtToPhys( const void *p ) {
	return (int)p < 0 ? ( (int)p & 0x1fffffffL ) :
		( unsigned int )( ( unsigned char * )p + 0x40000000L );
}

void delay_ms( uint32_t ms ) {
	uint32_t period = ms * MS_CYCLES;

	DisableIntT23;
	IFS0bits.T3IF = 0;
    OpenTimer23( T23_ON | T2_PS_1_64 | T2_32BIT_MODE_ON, period / 64 );
	while( IFS0bits.T3IF == 0 );
	CloseTimer23( );
}

void uint2be_16( uint16_t val, uint8_t *be ) {
	be[0] = ( val >> 8 ) & 0xFF;
	be[1] = ( val >> 0 ) & 0xFF;
}

void uint2be_32( uint32_t val, uint8_t *be ) {
	be[0] = _get_byte( val >> 24, 0 );
	be[1] = _get_byte( val >> 16, 0 );
	be[2] = _get_byte( val >>  8, 0 );
	be[3] = _get_byte( val >>  0, 0 );
}

uint16_t be2uint_16( const uint8_t *be ) {
	return ( ( be[0] << 8 )
			| ( be[1] << 0 ) );
}

uint32_t be2uint_32( const uint8_t *be ) {
	return ( ( be[0] << 24 )
			| ( be[1] << 16 )
			| ( be[2] <<  8 )
			| ( be[3] <<  0 ) );
}

uint32_t be2uint( const uint8_t *be, uint8_t n ) {
	uint32_t val = 0;
	int i;

	for( i = 0; i < n; ++i ) {
		val |= be[n-i-1] << (i<<3);
	}

	return val;
}

inline
uint16_t le2uint_16( const uint8_t *le ) {
	return ( le[0] | ( le[1] << 8 ) );
}

inline
uint32_t le2uint_32( const uint8_t *le ) {
	return ( le[0] | ( le[1] << 8 ) | ( le[2] << 16 ) | ( le[3] << 24 ) );
}
