#include <string.h>
#include <inttypes.h>
#include <GenericTypeDefs.h>

#include "carebox_common.h"
#include "util.h"
#include "exceptions.h"
#include "flash.h"
#include "flash_buf.h"
#include "flash_app.h"

#define flash_write(a,c,d)	flash_buf_write(a,c,d)
#define flash_erase(a,c)	flash_buf_erase(a,c)


/* sensor data record */
const flash_field_t f_flash	= { FLASH_BASE, FLASH_SIZE };

/* constant fields */
const flash_field_t f_magic	= { 0, 2 };
const flash_field_t f_uid	= { 2, 4 };
const flash_field_t f_const = { FLASH_CONST_BASE, 6 };

/* accident table */
const flash_field_t f_acctab	= { FLASH_ACCTAB_BASE, sizeof(flash_acctab_t) };

/* bookkeeping */
static BOOL			rec_acc = FALSE;
static flash_addr_t	rec_addr = FLASH_PAD_BASE;
static flash_addr_t	rec_addr_saved;


inline static
void flash_read_field( const flash_field_t *f, const flash_field_t *sub,
		void *val ) {
	flash_read( f->off + sub->off, sub->size, val );
}

inline static
void flash_write_field( const flash_field_t *f, const flash_field_t *sub,
		void *val ) {
	flash_write( f->off + sub->off, sub->size, val );
}

static uint8_t field_buf[ 64 ] = { 0 };
static
void flash_set_field( const flash_field_t *f, const flash_field_t *sub,
		const void *val ) {
	flash_addr_t addr = f->off;

	flash_read_field( &f_flash, f, field_buf );
	flash_erase( addr, f->size );
	memcpy( &field_buf[ sub->off ], val, sub->size );
	flash_write( addr, f->size, field_buf );
}

inline static
void flash_write_srec( flash_addr_t addr, flash_srec_t *rec ) {
	flash_write( addr, FLASH_ACC_SREC_SIZE, rec );
}


void flash_rec_init( void ) {
	flash_init( );
	if( flash_get_device( ) != FLASH_JEDEC_ID ) {
		cb_throw( ERR_FLASH, ERR_FLASH_ID );
	}

	/* clear write protection */
	flash_clr_protect( );

#if 1
	/* format flash if no magic found */
	if( flash_get_magic( ) != FLASH_MAGIC ) {
		const uint16_t magic = FLASH_MAGIC;
		const uint32_t uid = 0xCAFEBABE;
		const flash_acctab_t acc_tab = { 0, 0, FLASH_ACC_MAX_NO, 0xFF };

		flash_erase_all( TRUE );
		flash_raw_write( f_const.off + f_magic.off, f_magic.size, &magic );
		flash_raw_write( f_const.off + f_uid.off, f_uid.off, &uid );
		flash_raw_write( f_acctab.off, f_acctab.size, &acc_tab );
	}
#endif

	/* erase first pad sector */
	flash_raw_erase( rec_addr, FLASH_SECTOR_SIZE, TRUE );
}


uint16_t flash_get_magic( void ) {
	uint16_t magic;
	flash_read_field( &f_const, &f_magic, &magic );
	return magic;
}

void flash_set_magic( uint16_t magic ) {
	flash_set_field( &f_const, &f_magic, &magic );
}

flash_uid_t flash_get_uid( void ) {
	flash_uid_t uid;
	flash_read_field( &f_const, &f_uid, &uid );
	return uid;
}

void flash_set_uid( flash_uid_t uid ) {
	flash_set_field( &f_const, &f_uid, &uid );
}


inline
void flash_read_srec( flash_addr_t addr, flash_srec_t *rec ) {
	flash_read( addr, FLASH_ACC_SREC_SIZE, rec );
}

void flash_put_srec( const flash_srec_t *rec ) {
	flash_addr_t new_addr;

	flash_write( rec_addr, FLASH_ACC_SREC_SIZE, rec );

	new_addr = rec_addr + FLASH_ACC_SREC_SIZE;
	if( !rec_acc ) {
		if( FLASH_ACC_BASE - new_addr < FLASH_ACC_SREC_SIZE ) {
			new_addr = FLASH_PAD_BASE;
		}
		else if( !FLASH_IN_SAME_SECTOR( rec_addr, new_addr ) ) {
			new_addr = FLASH_PAD_ALIGN( new_addr );
			flash_erase( new_addr & FLASH_SECTOR_MASK, FLASH_SECTOR_SIZE );
		}
	}
	rec_addr = new_addr;
}

inline
void flash_get_srec( unsigned int acc, unsigned int n, flash_srec_t *srec ) {
	flash_addr_t addr = FLASH_ACC_REC2A( acc, n );
	flash_read_srec( addr, srec );
}


flash_addr_t flash_start_acc( void ) {
	flash_acctab_t acc_tab;
	flash_addr_t acc_addr;

	/* prepare for recording */
	flash_read_field( &f_flash, &f_acctab, &acc_tab );
	INC_WRAP( acc_tab.last, FLASH_ACC_MAX_NO );
	acc_addr = FLASH_ACC_I2A( acc_tab.last );
	flash_erase( acc_addr, FLASH_ACC_OCC );

	/* update state variables */
	rec_addr_saved = rec_addr;
	rec_addr = acc_addr;
	rec_acc = TRUE;

	return rec_addr_saved;
}

uint8_t flash_end_acc( void ) {
	flash_acctab_t acc_tab;

	/* update accident table */
	flash_read_field( &f_flash, &f_acctab, &acc_tab );
	if( acc_tab.count < FLASH_ACC_MAX_NO ) {
		++acc_tab.count;
	} else {
		INC_WRAP( acc_tab.first, FLASH_ACC_MAX_NO );
	}
	INC_WRAP( acc_tab.last, FLASH_ACC_MAX_NO );

	flash_erase( f_acctab.off, FLASH_ACCTAB_OCC );
	flash_write_field( &f_flash, &f_acctab, &acc_tab );

	rec_acc = FALSE;
	rec_addr = rec_addr_saved;

	return acc_tab.last;
}

uint8_t flash_get_last_acc( void ) {
	flash_acctab_t acc_tab;
	flash_read_field( &f_flash, &f_acctab, &acc_tab );
	return acc_tab.last;
}
