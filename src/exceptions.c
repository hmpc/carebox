#include <inttypes.h>
#include <GenericTypeDefs.h>

#include "pic_com.h"
#include "exceptions.h"

cb_error_t	last_err;
uint8_t		sub_err;
void cb_throw( cb_error_t err, uint8_t sub ) {
	uint8_t cerr[ ] = { err, sub };
//	pic_put_cmd( PIC_EXCEP, sizeof(cerr), cerr );
//	pic_send_cmd( );

	last_err = err;
	sub_err = sub;

	do {
		asm("nop");
	} while( 0 );
}
