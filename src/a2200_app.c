#include <inttypes.h>
#include <GenericTypeDefs.h>

#include "util.h"
#include "a2200_app.h"


struct _A2200 a2200_a;

const uint8_t start_seq[ ]	= { OSP_TSTART_1, OSP_TSTART_2 };
const uint8_t end_seq[ ]	= { OSP_TEND_1, OSP_TEND_2 };


void gps_write( uint32_t size, const uint8_t *data ) {
	int i;
	for( i = 0; i < size; ++i ) {
		(*a2200_a.byte_writer)( *data++ );
	}
}

uint32_t gps_read( uint32_t size, uint8_t *data ) {
	int i;
	for( i = 0; i < size; ++i ) {
		data[i] = (*a2200_a.byte_reader)( );
	}
	return size;
}

uint16_t gps_osp_chksum( uint16_t length, const uint8_t *payload ) {
	uint16_t chksum = 0;
	int i;
	for( i = 0; i < length; ++i ) {
		chksum = ( chksum + payload[i] ) & OSP_CHKSUM_MASK;
	}
	return chksum;
}

BOOL gps_osp_send( uint16_t length, const uint8_t *payload ) {
	osp_chksum_t chksum;
	uint8_t len[2], chk[2];

	uint2be_16( length, len );

	chksum = gps_osp_chksum( length, payload );
	uint2be_16( chksum, chk );

	gps_write( sizeof start_seq, start_seq );
	gps_write( sizeof length, len );
	gps_write( length, payload );
	gps_write( sizeof chksum, chk );
	gps_write( sizeof end_seq, end_seq );

	return TRUE;
}

inline
BOOL gps_osp_recv( uint16_t *len, uint8_t *data ) {
	uint8_t hdr_data[2] = { 0, 0 };
	osp_chksum_t tx_chksum, rx_chksum;

	/* wait for start sequence */
	while( hdr_data[0] != start_seq[0] || hdr_data[1] != start_seq[1] ) {
		hdr_data[0] = hdr_data[1];
		gps_read( 1, &hdr_data[1] );
	}

	/* read payload length */
	gps_read( 2, hdr_data );
	*len = be2uint_16( hdr_data );

	/* read payload */
	gps_read( *len, data );

	/* read transmitted payload checksum */
	gps_read( 2, hdr_data );
	tx_chksum = be2uint_16( hdr_data );

	/* read end sequence */
	gps_read( 2, hdr_data );

	/* verify payload checksum and end sequence */
	rx_chksum = gps_osp_chksum( *len, data );
	if( rx_chksum != tx_chksum ) {
		return FALSE;
	}

	if( hdr_data[0] != end_seq[0] || hdr_data[1] != end_seq[1] ) {
		return FALSE;
	}

	return TRUE;
}

BOOL gps_init( void ) {
	a2200_a.byte_writer = NULL;
	a2200_a.byte_reader = NULL;
	a2200_a.delay_ms = NULL;
	a2200_a.set_onoff = NULL;

	a2200_a.protocol = GPS_NMEA;
}

BOOL gps_on( gps_protocol_t proto ) {
	(*a2200_a.set_onoff)( TRUE );
	(*a2200_a.delay_ms)( A2200_TOGGLE );
	(*a2200_a.set_onoff)( FALSE );

	if( proto == a2200_a.protocol) {
		return TRUE;
	} else if( proto == GPS_OSP ) {
		gps_write( sizeof( NMEA_SWITCH_OSP ) - 1, NMEA_SWITCH_OSP );
	} else {
		return FALSE;
	}

	a2200_a.protocol = proto;
	return TRUE;
}

BOOL gps_f2a( const osp_msg_field_t *field, uint8_t *data ) {
	const uint8_t byte_off[ ] = {
		[OSP_DISCRETE]	= sizeof( osp_discrete_t ),
		[OSP_UNSIGNED]	= sizeof( osp_unsigned_t ),
		[OSP_SIGNED]	= sizeof( osp_signed_t ),
		[OSP_SINGLE]	= sizeof( osp_single_t ),
		[OSP_DOUBLE]	= sizeof( osp_double_t )
	};

	uint8_t bytes[ OSP_MAX_FIELD ];
	uint8_t *byte;
	uint8_t off;
	int i;

	off = OSP_MAX_FIELD - byte_off[field->type];
	byte = &bytes[off];

	switch( field->type ) {
		case OSP_DISCRETE:
			*(osp_discrete_t *)byte = field->value.d;
			break;

		case OSP_UNSIGNED:
			*(osp_unsigned_t *)byte = field->value.u;
			break;

		case OSP_SIGNED:
			*(osp_signed_t *)byte = field->value.s;
			break;

		case OSP_SINGLE:
			*(osp_single_t *)byte = field->value.sgl;
			break;

		case OSP_DOUBLE:
			*(osp_double_t *)byte = field->value.dbl;
			break;

		default:
			return FALSE;
	}

	for( i = 0; i < field->size; ++i ) {
		data[i] = bytes[ OSP_MAX_FIELD - i ];
	}

	return TRUE;
}

BOOL gps_a2f( uint8_t *data, osp_msg_field_t *field ) {
	uint8_t size = field->size;
	uint64_t val = be2uint( data, size );

	switch( field->type ) {
		case OSP_DISCRETE:
			field->value.d = *( osp_discrete_t * )&val;
			break;

		case OSP_UNSIGNED:
			field->value.u = *( osp_unsigned_t * )&val;
			break;

		case OSP_SIGNED:
			field->value.s = *( osp_signed_t * )&val;
			break;

		case OSP_SINGLE:
			field->value.sgl = *( osp_single_t * )&val;
			break;

		case OSP_DOUBLE:
			field->value.dbl = *( osp_double_t * )&val;
			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL gps_send( const osp_msg_t *msg ) {
	uint8_t n = msg->n_fields;
	osp_msg_field_t *fields = msg->fields;

	uint8_t buf[ n*OSP_MAX_FIELD ];
	int i, b;

	for( i = 0, b = 0; i < n; ++i ) {
		if( gps_f2a( &fields[i], &buf[b] ) == FALSE ) {
			return FALSE;
		}

		b += fields[i].size;
	}

	return gps_osp_send( b, buf );
}


static uint8_t msg_buf[ OSP_MAX_MSG ];
BOOL gps_recv( osp_msg_t *msg ) {
	if( msg == NULL ) {
		return FALSE;
	}

	uint8_t mid = msg->mid;
	uint8_t n = msg->n_fields;
	uint16_t len;
	BOOL ret = FALSE;

	msg_buf[0] = 0;
	while( msg_buf[0] != mid || ret == FALSE ) {
		ret = gps_osp_recv( &len, msg_buf );
	}

	uint8_t i, *b;
	for( i = 0, b = msg_buf + 1; i < n && b - msg_buf < len; ++i ) {
		if( msg->fields[i].type != OSP_DUMMY ) {
			if( gps_a2f( b, &msg->fields[i] ) == FALSE ) {
				return FALSE;
			}
		}

		b += msg->fields[i].size;
	}

	return TRUE;
}

static osp_msg_field_t nav_fields[17];
BOOL gps_get_nav( gps_nav_t *nav_data ) {
	const osp_mid_t nav_mid = OSP_OUT_GEO_DATA;
	const uint8_t n_fields = 17;

	osp_msg_t nav_msg;

	/* Geodetic Navigation Data message definition */
	OSP_MSG_CREATE( nav_msg, nav_mid, n_fields, nav_fields );
	OSP_MSG_FIELD( nav_msg,  0, 2, OSP_DISCRETE );	/* Nav Valid */
	OSP_MSG_FIELD( nav_msg,  1, 2, OSP_DISCRETE );	/* Nav Type */
	OSP_MSG_FIELD( nav_msg,  2, 2, OSP_UNSIGNED );	/* Extended Week Number */
	OSP_MSG_FIELD( nav_msg,  3, 4, OSP_UNSIGNED );	/* Time Of Week */
	OSP_MSG_FIELD( nav_msg,  4, 2, OSP_UNSIGNED );	/* UTC Year */
	OSP_MSG_FIELD( nav_msg,  5, 1, OSP_UNSIGNED );	/* UTC Month */
	OSP_MSG_FIELD( nav_msg,  6, 1, OSP_UNSIGNED );	/* UTC Day */
	OSP_MSG_FIELD( nav_msg,  7, 1, OSP_UNSIGNED );	/* UTC Hour */
	OSP_MSG_FIELD( nav_msg,  8, 1, OSP_UNSIGNED );	/* UTC Minute */
	OSP_MSG_FIELD( nav_msg,  9, 2, OSP_UNSIGNED );	/* UTC Second */
	OSP_MSG_FIELD( nav_msg, 10, 4, OSP_DISCRETE );	/* Satellite ID List */
	OSP_MSG_FIELD( nav_msg, 11, 4, OSP_SIGNED );	/* Latitude */
	OSP_MSG_FIELD( nav_msg, 12, 4, OSP_SIGNED );	/* Longitude */
	OSP_MSG_FIELD( nav_msg, 13, 4, OSP_SIGNED );	/* Alt from Ellipsoid */
	OSP_MSG_FIELD( nav_msg, 14, 4, OSP_SIGNED );	/* Altitude from MSL */
	OSP_MSG_FIELD( nav_msg, 15, 1, OSP_SIGNED );	/* Map Datum */
	OSP_MSG_FIELD( nav_msg, 16, 2, OSP_UNSIGNED );	/* Speed Over Ground */

	if( gps_recv( &nav_msg ) == FALSE ) {
		return FALSE;
	}

	OSP_MSG_GET( nav_msg,  0, nav_data->valid );
	OSP_MSG_GET( nav_msg,  1, nav_data->type );
	OSP_MSG_GET( nav_msg,  2, nav_data->week );
	OSP_MSG_GET( nav_msg,  3, nav_data->tow );
	OSP_MSG_GET( nav_msg,  4, nav_data->time.year );
	OSP_MSG_GET( nav_msg,  5, nav_data->time.month );
	OSP_MSG_GET( nav_msg,  6, nav_data->time.day );
	OSP_MSG_GET( nav_msg,  7, nav_data->time.hour );
	OSP_MSG_GET( nav_msg,  8, nav_data->time.minute );
	OSP_MSG_GET( nav_msg,  9, nav_data->time.second );
	OSP_MSG_GET( nav_msg, 10, nav_data->sats );
	OSP_MSG_GET( nav_msg, 11, nav_data->geo_pos[0] );
	OSP_MSG_GET( nav_msg, 12, nav_data->geo_pos[1] );
	OSP_MSG_GET( nav_msg, 14, nav_data->geo_pos[2] );
	OSP_MSG_GET( nav_msg, 15, nav_data->datum );
	OSP_MSG_GET( nav_msg, 16, nav_data->sog );

	return TRUE;
}

