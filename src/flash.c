#include <plib.h>
#include <inttypes.h>

#include "carebox_common.h"
#include "util.h"
#include "exceptions.h"
#include "flash.h"


void flash_config_io( void ) {
    SDI1R = 0x01; // RPB5
    RPA4R = 0x03; // SDO1
}

void flash_init() {
    uint8_t rData;

    FLASH_CS_TRIS = 0;
    FLASH_SDI_TRIS = 0;
    FLASH_SDO_TRIS = 1;
    FLASH_SCK_TRIS = 0;

    FLASH_SDI = 0;
    FLASH_CS = 1;

    IEC0CLR = 0x03800000;

    SPI1CON = 0;
    
    rData = SPI1BUF;
    
    SPI1BRG = 85;//FPB/(2*FLASH_SPI_FREQ) - 1;
    SPI1STATbits.SPIROV = 0;

    SPI1CONbits.MSTEN = 1;
    SPI1CONbits.ON = 1;
}

static
void flash_put( uint8_t data ) {
	uint8_t dummy;
	while ( !SPI1STATbits.SPITBE );
	SPI1BUF = data;
	while ( !SPI1STATbits.SPIRBF );
	dummy = SPI1BUF;
}

static
uint8_t flash_get( ) {
	while ( !SPI1STATbits.SPITBE );
	SPI1BUF = 0x00;
	while ( !SPI1STATbits.SPIRBF );
	return SPI1BUF;
}

static
void flash_write_en( BOOL en ) {
    FLASH_CS = 0;
    flash_put( en ? FLASH_WREN : FLASH_WRDI );
    FLASH_CS = 1;
}

static
void flash_put_addr( flash_addr_t addr ) {
	flash_put( (addr & 0xFFFFFF) >> 16 );
	flash_put( (addr & 0xFFFF ) >> 8  );
	flash_put( (addr & 0xFF ) >> 0 );
}

void flash_read( flash_addr_t addr, uint32_t count, void *data ) {
	int i;

	FLASH_CS = 0;
	flash_put( FLASH_READ );
	flash_put_addr( addr );
	for( i = 0; i < count; ++i ) {
		(( uint8_t * )data)[i] = flash_get( );
	}
	FLASH_CS = 1;
}

void flash_write_byte( flash_addr_t addr, uint8_t data ) {
	flash_write_en( TRUE );
	FLASH_CS = 0;

	flash_put( FLASH_BYTE_PROGRAM );
	flash_put_addr( addr );
	flash_put( data );

	FLASH_CS = 1;
	flash_write_en( FALSE );
}

static
void flash_eow( BOOL en ) {
    FLASH_CS = 0;
    flash_put( en ? FLASH_EBSY : FLASH_DBSY );
    FLASH_CS = 1;
}

void flash_raw_write( flash_addr_t addr, uint32_t count, const void *data ) {
	uint8_t i;

	flash_eow( TRUE );
	flash_write_en( TRUE );

	FLASH_CS = 0;

	flash_put( FLASH_AAI );
	flash_put_addr( addr );

	for( i = 0; i < count/2; i++ ) {
		flash_put( (( uint8_t * )data)[2*i] );
		flash_put( (( uint8_t * )data)[2*i+1] );

		FLASH_CS = 1;
		CYCLE_DELAY( 400 );
		while( !FLASH_HW_EOW );
		FLASH_CS = 0;

		if( i+1 < count/2 ) {
			flash_put( FLASH_AAI );
		}
	}

	FLASH_CS = 1;

	flash_write_en( FALSE );
	flash_eow( FALSE );
}

uint8_t flash_read_status() {
    uint8_t s;
    FLASH_CS = 0;
    flash_put( FLASH_RDSR );
    s = flash_get();
    FLASH_CS = 1;
    return s;
}

void flash_write_status( uint8_t status ) {
	flash_write_en( TRUE );
	FLASH_CS = 0;
	flash_put( FLASH_WRSR );
	flash_put( status );
	FLASH_CS = 1;
}

BOOL flash_is_busy( void ) {
	return ( flash_read_status( ) & FLASH_SR_BUSY != 0 );
}

void flash_clr_protect( void ) {
	uint8_t status = flash_read_status( );
	status &= ~( FLASH_SR_BP0 | FLASH_SR_BP1 );
	flash_write_status( status );
}

static
void flash_erase_sector( flash_addr_t addr, flash_block_t bt, BOOL blocking ) {
	const uint8_t cmd[ ] = { FLASH_4K_ERASE, FLASH_32K_ERASE, FLASH_64K_ERASE };

	flash_write_en( TRUE );
	FLASH_CS = 0;

	flash_put( cmd[bt] );
	flash_put_addr( addr );

	FLASH_CS = 1;
	while( blocking && flash_is_busy( ) );
	flash_write_en( FALSE );
}

void flash_raw_erase( flash_addr_t start, uint32_t size, BOOL blocking ) {
	if( size <= FLASH_SECTOR_SIZE ) {
		flash_erase_sector( start, FLASH_SECTOR, blocking );
	} else if( size <= FLASH_BLOCK32_SIZE ) {
		flash_erase_sector( start, FLASH_BLOCK_32, blocking );
	} else if( size <= FLASH_BLOCK64_SIZE ) {
		flash_erase_sector( start, FLASH_BLOCK_64, blocking );
	} else {
		cb_throw( ERR_FLASH, ERR_FLASH_ERASE );
	}
}

void flash_erase_all( BOOL blocking ) {
	flash_write_en( TRUE );
	FLASH_CS = 0;
	flash_put( FLASH_CHIP_ERASE );
	FLASH_CS = 1;
	while( blocking && flash_is_busy( ) );
	flash_write_en( FALSE );
}

uint32_t flash_get_device( void ) {
	uint32_t id;

	FLASH_CS = 0;
	flash_put( FLASH_READID );
	id = flash_get();
	id = ( id << 8 ) | flash_get();
	id = ( id << 8 ) | flash_get();
	FLASH_CS = 1;

	return id;
}

