#include <inttypes.h>
#include <GenericTypeDefs.h>
#include <plib.h>

#include "carebox_common.h"
#include "a2200_uart.h"


uint8_t	osp_buf[ OSP_MAX_BUF ];
uint8_t	rd_ptr = 0;


void uart_config_io( void ) {
	U1RXR = 0x01;
	RPB7R = 0x01;
}

void uart_init( void ) {
	/* setup auto-enabled, UART IRQ-driven DMA channel */
	mDmaEnable( );
//	DmaChnOpen( OSP_DMA_CHN, OSP_DMA_PRI, DMA_OPEN_AUTO );
	DCH1CONbits.CHAEN = 1;
	DCH1CONbits.CHPRI = OSP_DMA_PRI;
//	DmaChnSetTxfer( OSP_DMA_CHN, ( void * )&U1RXREG, osp_buf,
//			1, OSP_MAX_BUF, 1 );
	DCH1SSA = VirtToPhys( &U1RXREG );
	DCH1DSA = VirtToPhys( osp_buf );
	DCH1SSIZ = 1;
	DCH1DSIZ = OSP_MAX_BUF;
	DCH1CSIZ = 1;
//	DmaChnSetEventControl( OSP_DMA_CHN, DMA_EV_START_IRQ_EN |
//			DMA_EV_START_IRQ(_UART1_RX_IRQ) );
	DCH1ECON = DMA_EV_START_IRQ_EN | DMA_EV_START_IRQ(_UART1_RX_IRQ);
//	DmaChnEnable( OSP_DMA_CHN );
	DCH1CONbits.CHEN = 1;

	/* setup UART */
	U1MODEbits.BRGH = 0;
	OpenUART1( UART_EN | UART_NO_PAR_8BIT | UART_1STOPBIT,
			UART_TX_PIN_NORMAL | UART_TX_ENABLE | UART_RX_ENABLE,
			FPB/OSP_BAUD/16 - 1 );
	U1STAbits.UTXINV = 0;
	U1MODEbits.RXINV = 0;
}

uint8_t uart_read( void ) {
	if( rd_ptr >= OSP_MAX_BUF ) {
		rd_ptr = 0;
	}
//	while( rd_ptr > DmaChnGetDstPnt( OSP_DMA_CHN ) );
	while( rd_ptr > (DCH1DPTR & 0xFF) );
	return osp_buf[ rd_ptr++ ];
}

void uart_write( uint8_t byte ) {
	while( BusyUART1( ) );
	WriteUART1( byte );
}

